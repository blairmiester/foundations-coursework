@SuppressWarnings("rawtypes")
public class Type
{
	public Tree value1;
	public int value2;
	public Pair value3;
	
	
	public Type(Tree value1)
	{
		this.value1 = value1;
	}
	
	public Type(int value2)
	{
		this.value2 = value2;
	}
	
	public Type(Pair value3)
	{
		this.value3 = value3;
	}

	public Tree getSet() 
	{
        return value1;
    }
	
	public int getInt() 
	{
        return value2;
    }
	
	public Pair getPair() 
	{
        return value3;
    }
}