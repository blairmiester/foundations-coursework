 @SuppressWarnings("unchecked")
 
public class Pair 
{ 
    public final Type x; 
    public final Type y; 
    public Pair(Type x, Type y) 
    { 
        this.x = x; 
        this.y = y; 
    }
    
    public Type getX(Type x)
    {
    	return x;
    }
    
    public Type getY(Type y)
    {
    	return y;
    }

        
    public void print()
    {
    	System.out.print("(");
    	System.out.print(getX(x));
    	System.out.print(",");
    	System.out.print(getY(y));
    	System.out.print(")");
    }

    @Override
    public boolean equals(Object other) 
    {
        if (other == null) 
        {
            return false;
        }
        if (other == this) 
        {
            return true;
        }
        if (!(other instanceof Pair))
        {
            return false;
        }
       
		Pair other_ = (Pair) other;
        return other_.x == this.x && other_.y == this.y;
    }

    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((x == null) ? 0 : x.hashCode());
        result = prime * result + ((y == null) ? 0 : y.hashCode());
        return result;
    }
}