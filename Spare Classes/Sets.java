import java.util.*;

public class Sets<Type> extends LinkedHashSet
{	
	public void print()
	{
		Iterator<Type> it = iterator();
	    if (! it.hasNext())
	        System.out.print("{}");
	    
	    System.out.print("{");
	    /*
	    for (;;) 
	    {
	        Type e = it.next();
	        System.out.print(e);
	        //System.out.print(",");
	        if (! it.hasNext())
	        {
	            System.out.print("}");
	        }
	        //System.out.print(", ");
	    }
	    
	    Type e = it.next();
	    	System.out.print(e);
	    	System.out.print(",");
	    */
	    
	    while(it.hasNext())
	    {
	    	Type e = it.next();
	    	
	    	if(e instanceof Pair)
	    	{
	    		((Pair) e).print();
	    	}
	    	else
	    		if(e instanceof Sets)
	    		{
	    			Iterator<Type> sub = iterator();
	    			Type f = sub.next();
	    			
	    			if(f instanceof Pair)
	    	    	{
	    	    		((Pair) f).print();
	    	    	}
	    			
	    			else
		    			if(f instanceof Integer)
		    			{
		    				System.out.print(f);
		    				System.out.print(", ");
		    			}
	    			
	    		}
	    		else
	    			if(e instanceof Integer)
	    			{
	    				
	    			}
	    	
	    }
	    System.out.print("}");
	    
	}
}