import java.util.LinkedHashSet;
import java.util.Set;
@SuppressWarnings("unchecked")

public class operations
{
  public static <T> Set<T> union(Set<T> setA, Set<T> setB) 
  {
    Set<T> tmp = new Sets<T>();
    tmp.addAll(setA);
    tmp.addAll(setB);
    return tmp;
  }

  public static <T> Set<T> intersection(Set<T> s1, Set<T> s2) 
  {
	   
		Set<T> intersection = new Sets<T>();
	    intersection.addAll(s1);
	    intersection.retainAll(s2);
	    return intersection;
  }

  public static <T> Set<T> difference(Set<T> setA, Set<T> setB) 
  {
    Set<T> tmp = new Sets<T>();
    tmp.addAll(setA);
    tmp.removeAll(setB);
    return tmp;
  }

  public static <T> Set<T> symDifference(Set<T> setA, Set<T> setB) 
  {
    Set<T> tmpA;
    Set<T> tmpB;

    tmpA = union(setA, setB);
    tmpB = intersection(setA, setB);
    return difference(tmpA, tmpB);
  }

  public static <T> boolean isSubset(Set<T> setA, Set<T> setB) 
  {
    return setB.containsAll(setA);
  }

  public static <T> boolean isSuperset(Set<T> setA, Set<T> setB) 
  {
    return setA.containsAll(setB);
  }
}