import java.util.*;


public class operation 
{
	public static Tree union(Tree A,Tree B)
	{
	        Iterator<Tree> i = B.nodes();
	        while(i.hasNext())
	        {
	                //We then add all the Children Trees (which are elements of the Set) to the first Tree
	                A.addNode(i.next());
	        }
	        return A;
	}

	public static Tree subtraction(Tree A,Tree B)
	{
	        LinkedList<Tree> node_b = new LinkedList<Tree>();
	        Iterator<Tree> i = B.nodes();
	        while(i.hasNext())
	        {
	                //we then add all the nodes from our second Tree to the linkedlist
	                node_b.add(i.next());
	        }

	        Iterator<Tree> second = A.nodes();

	        Tree answer = new Tree("Set");
	        
	        while(second.hasNext())
	        {
	                //compare the two sets and add to our new tree if tree B doesn't contain an exact element
	                Tree this_one = second.next();
	                if(!node_b.contains(this_one))
	                {
	                        answer.addNode(this_one);
	                }
	        }
	        return answer;
	}

	public static Tree intersection(Tree A,Tree B)
	{ 
	        LinkedHashSet<Tree> node_a = new LinkedHashSet<Tree>();
	        Iterator<Tree> i = A.nodes();
	        while(i.hasNext())
	        {
	                //We add all the elements from our first Tree to our set
	                node_a.add(i.next());
	        }

	        Iterator<Tree> second = B.nodes();
	        //We create A new Tree to represent a Set
	        Tree answer = new Tree("Set");
	        while(second.hasNext())
	        {
	                //If the element is in both sets, we add the element to our Result Set
	                Tree this_one = second.next();
	                if(node_a.contains(this_one))
	                {
	                        answer.addNode(this_one);
	                }
	        }
	        //We return our Tree as the result of the Intersection
	        return answer;
	}
	
	public static void print(Tree node)
	{
	        //This Method prints out the Notation as it should appear
	        //It checks what type the Tree is and then does an action depending on the action
	        if(node.getType().equals("Integer"))
	        {
	                //If an Integer, We will just print it out
	                System.out.print(node.getValue());
	        }
	        else if(node.getType().equals("Pair"))
	        {
	                //If it's A Pair, we will put () around the numbers stored as Children and recursively call the print method.
	                System.out.print("(");
	                print(node.getNodeAt(0));
	                System.out.print(",");
	                print(node.getNodeAt(1));
	                System.out.print(")");
	        }
	        else if(node.getType().equals("Set"))
	        {
	                //If the Node is storing a set we will do the following:-
	                //Open A Curly Bracket
	                //Print Each element (by recursively calling the print method) in the array followed by A comma if there is another element after it
	                //Close the Set with a Curly Bracket
	                System.out.print("{");
	                Iterator<Tree> i = node.nodes();
	                while(i.hasNext())
	                {
	                        print(i.next());
	                        if(i.hasNext())
	                        {
	                                System.out.print(",");
	                        }
	                }
	                System.out.print("}");
	        }
	}
}
