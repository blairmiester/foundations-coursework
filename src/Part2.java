import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
 
public class Part2 
{

	static HashMap<Object, Object> lhm = new HashMap<Object, Object>();
	static LinkedList<Object> ll = new LinkedList<Object>();
 
  public static void run()
  {
	  
	  
    try 
    {
		File file = new File("simple-input.xml");
	 
		DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance()
												.newDocumentBuilder();
	 
		Document doc = dBuilder.parse(file);
	 
		System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

		if (doc.hasChildNodes()) 
		{
			printNodes(doc.getChildNodes());
			/*
			printNodes(doc.getElementsByTagName("equal"));
			printNodes(doc.getElementsByTagName("integer"));
			printNodes(doc.getElementsByTagName("set"));
			printNodes(doc.getElementsByTagName("tuple"));
			printNodes(doc.getElementsByTagName("member"));
			*/
		}
		 
		
		
	 
	    } 
	    catch (Exception e) 
	  	{
	    	System.out.println(e.getMessage());
	    }
    
    //System.out.println(lhm.toString());
    ll.remove(0);
    System.out.println(ll.toString());
    
    Tree x0 = new Tree("Integer",ll.get(1));
    lhm.put(0, x0);
    
    Tree x1 = new Tree("Integer",ll.get(4));
    lhm.put(1, x1);
    	
    
    
    Iterator<Object> printTree = lhm.keySet().iterator();
    
    
    while(printTree.hasNext())
    {
    	Integer key = (Integer) printTree.next();
    	
    	System.out.print("x"+ key +" = ");
    	
    	operation.print((Tree) lhm.get(key));
    	
    	System.out.println();	
    }
    
 
  	}
 
  private static void printNodes(NodeList nodeList) throws IOException 
  {
	  
    for (int count = 0; count < nodeList.getLength(); count++) 
    {
 
		Node tempNode = nodeList.item(count);
	 
		// make sure it's element node.
		if (tempNode.getNodeType() == Node.ELEMENT_NODE) 
		{
	 
			// get node name and value
			System.out.println("\nNode Name =" + tempNode.getNodeName() + " [OPEN]");
			System.out.println("Node Value =" + tempNode.getTextContent());
			//ll.add("\nNode Name =" + tempNode.getNodeName() + " [OPEN]");
			//ll.add("Node Value =" + tempNode.getTextContent());
			//ll.add(tempNode.getTextContent());
			
			
			if(tempNode.getNodeName().equalsIgnoreCase("equal"))
			{
				ll.add(tempNode.getTextContent());	
			}
			else
				if(tempNode.getNodeName().equalsIgnoreCase("integer"))
				{
					ll.add(tempNode.getTextContent());
				}
				else
					if(tempNode.getNodeName().equalsIgnoreCase("variable"))
					{
						ll.add(tempNode.getTextContent());
					}
					else
						if(tempNode.getNodeName().equalsIgnoreCase("set"))
						{
							ll.add(tempNode.getTextContent());
						}
						else
							if(tempNode.getNodeName().equalsIgnoreCase("tuple"))
							{
								ll.add(tempNode.getTextContent());
							}
							else
								if(tempNode.getNodeName().equalsIgnoreCase("member"))
								{
									ll.add(tempNode.getTextContent());
								}
	 
			
	 
			if (tempNode.hasChildNodes()) 
			{
	 
				// loop again if has child nodes
				printNodes(tempNode.getChildNodes());
	 
			}
			else
			{
				lhm.put(tempNode.getNodeName() + count, tempNode.getTextContent());
			}

			System.out.println("Node Name =" + tempNode.getNodeName() + " [CLOSE]");
			//ll.add("\nNode Name =" + tempNode.getNodeName() + " [CLOSE]");
			
		}
    } 
  }
}




/*
if (tempNode.hasAttributes()) 
{

	// get attributes names and values
	NamedNodeMap nodeMap = tempNode.getAttributes();

	for (int i = 0; i < nodeMap.getLength(); i++) 
	{

		Node node = nodeMap.item(i);
		System.out.println("attr name : " + node.getNodeName());
		System.out.println("attr value : " + node.getNodeValue());

	}

}
*/
 




