import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import com.google.gson.Gson;


public class Part1 
{
	public static void run()
	{
		HashMap<Integer, Tree> x = new HashMap<Integer, Tree>();

        Tree x0 = new Tree("Integer",8);
        x.put(0, x0);

        Tree x1 = new Tree("Set");
        x1.addNode(new Tree("Integer",1));
        x1.addNode(new Tree("Integer",2));
        x1.addNode(new Tree("Integer",3));
        x1.addNode(new Tree("Integer",4));
        x1.addNode(new Tree("Integer",5));
        x1.addNode(new Tree("Integer",6));
        x1.addNode(new Tree("Integer",7));
        x1.addNode(new Tree("Integer",8));
        x.put(1, x1);

        
        Tree x2 = new Tree("Set");
        x2.addNode(x1);
        
        Tree x2P = new Tree("Pair");
        x2P.addNode(new Tree("Integer",1));
        x2P.addNode(x1);
        x2.addNode(x2P);
        x.put(2, x2);
        
        Tree x3 = new Tree("Pair");
        x3.addNode(x2);
        x3.addNode(x1);
        x.put(3, x3);

        Tree x4 = new Tree("Set");
        x4.addNode(x3);
        x4 = operation.union(x4,x2);
        x.put(4, x4);

        Tree x5 = new Tree("Set");
        x5.addNode(x1);
        x5 = operation.subtraction(x4,x5);
        x.put(5, x5);

        Tree x6 = new Tree("Set");
        x6.addNode(x1);
        x6 = operation.intersection(x4,x6);
        x.put(6, x6);

        
        Iterator<Integer> printTree = x.keySet().iterator();
        
        while(printTree.hasNext())
        {
        	Integer key = (Integer) printTree.next();
        	
        	System.out.print("x"+ key +" = ");
        	
        	operation.print((Tree) x.get(key));
        	
        	System.out.println();	
        }
        
        
		Gson gson = new Gson();
	 
		// convert java object to JSON format,
		// and returned as JSON formatted string
		String json = gson.toJson(x);
	 
		try {
			//write converted json data to a file named "file.json"
			FileWriter writer = new FileWriter("meh.json");
			writer.write(json);
			writer.close();
	 
		} catch (IOException e) {
			e.printStackTrace();
		}
	 
		System.out.println(json);
	}
}
