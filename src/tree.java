import java.util.*;

public class Tree 
{
	
	private String type; //What is being stored?
	private Object value; //If our Tree holds an Integer, it will be stored here
	private LinkedList<Tree> node; //Node Elements of Sets and Pairs are sorted in this LinkedList
	
	public Tree(String type,Object value)
	{
		//This Constructor is used for Integers
		this.type = type;
		this.value = value;
		node = new LinkedList<Tree>();
	}
	
	public Tree(String type)
	{
		//This Constructor is for Sets and Pairs
		this.type = type;
		node = new LinkedList<Tree>();
	}

	public Iterator<Tree> nodes() 
	{
		//an iterator for all the nodes
		return node.iterator();
	}
	
	//add and remove nodes
	
	public void addNode(Tree x)
	{
		node.add(x);
	}
	
	public void removeNodeAt(int position)
	{
		node.remove(position);
	}
	
	//Returns how many node are linked to this Tree
	
	public Object getNodeCount() 
	{
		return node.size();
	}
	
	//Getters and Setters for the Tree
	public String getType() 
	{
		return type;
	}

	public Tree getNodeAt(int position) 
	{
		return node.get(position);
	}
	
	public Object getValue()
	{
		return this.value;
	}
	
	public void setValue(Object e)
	{
		this.value = e;
	}
}